function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

const current = Vue.ref(0);
const first = Vue.ref();
const second = Vue.ref();



const app = Vue.createApp({
    setup(){
        const found = Vue.ref(0);
        const turns = Vue.ref(0);
        const cards = Vue.ref(
        shuffle([
            {name: '?', image: './img/complete/1.jpg', turned: false, id: 1, disabled: false},
            {name: '?', image: './img/complete/1.jpg', turned: false, id: 1, disabled: false},
            {name: '?', image: './img/complete/2.jpg', turned: false, id: 2, disabled: false},
            {name: '?', image: './img/complete/2.jpg', turned: false, id: 2, disabled: false},
            {name: '?', image: './img/complete/3.jpg', turned: false, id: 3, disabled: false},
            {name: '?', image: './img/complete/3.jpg', turned: false, id: 3, disabled: false},
            {name: '?', image: './img/complete/4.jpg', turned: false, id: 4, disabled: false},
            {name: '?', image: './img/complete/4.jpg', turned: false, id: 4, disabled: false},
            {name: '?', image: './img/complete/5.jpg', turned: false, id: 5, disabled: false},
            {name: '?', image: './img/complete/5.jpg', turned: false, id: 5, disabled: false},
            {name: '?', image: './img/complete/6.jpg', turned: false, id: 6, disabled: false},
            {name: '?', image: './img/complete/6.jpg', turned: false, id: 6, disabled: false},
            {name: '?', image: './img/complete/7.jpg', turned: false, id: 7, disabled: false},
            {name: '?', image: './img/complete/7.jpg', turned: false, id: 7, disabled: false},
            {name: '?', image: './img/complete/8.jpg', turned: false, id: 8, disabled: false},
            {name: '?', image: './img/complete/8.jpg', turned: false, id: 8, disabled: false},
            {name: '?', image: './img/complete/9.jpg', turned: false, id: 9, disabled: false},
            {name: '?', image: './img/complete/9.jpg', turned: false, id: 9, disabled: false},
            {name: '?', image: './img/complete/10.jpg', turned: false, id: 10, disabled: false},
            {name: '?', image: './img/complete/10.jpg', turned: false, id: 10, disabled: false},
            {name: '?', image: './img/complete/11.jpg', turned: false, id: 11, disabled: false},
            {name: '?', image: './img/complete/11.jpg', turned: false, id: 11, disabled: false},
            {name: '?', image: './img/complete/12.jpg', turned: false, id: 12, disabled: false},
            {name: '?', image: './img/complete/12.jpg', turned: false, id: 12, disabled: false},
            {name: '?', image: './img/complete/13.jpg', turned: false, id: 13, disabled: false},
            {name: '?', image: './img/complete/13.jpg', turned: false, id: 13, disabled: false},
            {name: '?', image: './img/complete/14.jpg', turned: false, id: 14, disabled: false},
            {name: '?', image: './img/complete/14.jpg', turned: false, id: 14, disabled: false},
            {name: '?', image: './img/complete/15.jpg', turned: false, id: 15, disabled: false},
            {name: '?', image: './img/complete/15.jpg', turned: false, id: 15, disabled: false},
            {name: '?', image: './img/complete/16.jpg', turned: false, id: 16, disabled: false},
            {name: '?', image: './img/complete/16.jpg', turned: false, id: 16, disabled: false},
        ]));


        function turn (card){
            console.log("KORTTI:",card);
            if(current.value !== 999 && card.disabled !== true){
                card.turned = !card.turned;
            }
        }

        function compare(card){

            if(card.disabled == false){
                // First card
                if(current.value == 0 && current.value !== 999){
                    first.value = card;
    
                    console.log("Eka kortti");
    
                    current.value = card.id;
                    console.log("Current nyt:",current.value);
                }
                // Second card
                else if(current.value !== 999){
                    second.value = card;
    
                    console.log("Toka kortti");
                    // turns.value = 1;
                    // console.log(app.turns.value);
    
                    console.log("Eka ja toka", first.value.id, second.value.id);
    
                    if(first.value.id === second.value.id){
                        console.log("Pari löytyi!");
                        first.value.disabled = true;
                        second.value.disabled = true;
                        turns.value++;
                        found.value++;
                        current.value = 0;
                        console.log("Current nyt:", current.value);
                    }
                    else{
                        
                        console.log("Ei pari.");
                        turns.value++;
                        current.value = 999;
                        setTimeout(()=>{
                            first.value.turned = !first.value.turned;
                            second.value.turned = !second.value.turned;
                            current.value = 0;
                            console.log("Current nyt:", current.value);
    
                        },1000)
    
                    };
                }
            }

        }
        
        return { cards, turns, turn, compare, found};
    }
})

app.mount('#app')